we_universal
------------

we_universal is a modern, clean and simple style for phpBB 3.0.x that uses
principles of flat design and Responsive Web Design technique. Built with
HTML5 & CSS3. Fits great on desktops and various mobile devices. Suitable
for any kind of forum communities. Compatible with all major browsers.

Based on prosilver and built using template inheritance that simplifies
customising and updating. All new CSS classes are prefixed with "inventia-"
namespace and grouped in separate we_universal.css file.


FAQ and Documentation
---------------------

http://inventea.com/en/projects/we_universal/faq
http://inventea.com/en/projects/we_universal/documentation


Assets
------

- Header photo from picjumbo (http://picjumbo.com)
- Font Awesome 4.1.0 icons (http://fortawesome.github.io/Font-Awesome/)
- Zepto.js 1.1.4 library (http://zeptojs.com)
