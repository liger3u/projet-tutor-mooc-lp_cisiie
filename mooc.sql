-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mer 23 Mars 2016 à 00:45
-- Version du serveur :  5.7.9
-- Version de PHP :  5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `mooc`
--

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
CREATE TABLE IF NOT EXISTS `categorie` (
  `id_cat` int(11) NOT NULL AUTO_INCREMENT,
  `libelle_cat` varchar(60) NOT NULL,
  `id_mat` int(11) NOT NULL,
  PRIMARY KEY (`id_cat`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `categorie`
--

INSERT INTO `categorie` (`id_cat`, `libelle_cat`, `id_mat`) VALUES
(1, 'Geometrie dans l''espace', 1),
(2, 'Algorithme', 1),
(3, 'Algebre', 1);

-- --------------------------------------------------------

--
-- Structure de la table `compte`
--

DROP TABLE IF EXISTS `compte`;
CREATE TABLE IF NOT EXISTS `compte` (
  `id_com` smallint(11) NOT NULL AUTO_INCREMENT,
  `nom_com` varchar(60) NOT NULL,
  `prenom_com` varchar(60) NOT NULL,
  `mail_com` text NOT NULL,
  `pass_com` text NOT NULL,
  `adresse_com` text NOT NULL,
  `cp_com` int(5) NOT NULL,
  `ville_com` varchar(60) NOT NULL,
  `dateNaiss_com` date NOT NULL,
  `dateCreat_com` date NOT NULL,
  `id_etc` smallint(11) NOT NULL,
  `id_lic` smallint(11) NOT NULL,
  PRIMARY KEY (`id_com`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `compte`
--

INSERT INTO `compte` (`id_com`, `nom_com`, `prenom_com`, `mail_com`, `pass_com`, `adresse_com`, `cp_com`, `ville_com`, `dateNaiss_com`, `dateCreat_com`, `id_etc`, `id_lic`) VALUES
(5, 'Zidane', 'Zinedine', 'jojodal54@hotmail.fr', '7510d498f23f5815d3376ea7bad64e29', 'Non renseigne', 0, 'Non renseigne', '0000-00-00', '2016-01-15', 8, 2),
(6, 'test', 'root', 'test@example.fr', 'df5ea29924d39c3be8785734f13169c6', 'Non renseigne', 0, 'Non renseigne', '0000-00-00', '2016-01-24', 9, 1);

-- --------------------------------------------------------

--
-- Structure de la table `cours`
--

DROP TABLE IF EXISTS `cours`;
CREATE TABLE IF NOT EXISTS `cours` (
  `id_cou` smallint(11) NOT NULL AUTO_INCREMENT,
  `pdf_cou` text NOT NULL,
  `datePublie_cou` date NOT NULL,
  `heurePublie_cou` time NOT NULL,
  `id_com` smallint(11) NOT NULL,
  `id_gro` int(11) NOT NULL,
  `lvl` int(11) NOT NULL,
  PRIMARY KEY (`id_cou`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `cours`
--

INSERT INTO `cours` (`id_cou`, `pdf_cou`, `datePublie_cou`, `heurePublie_cou`, `id_com`, `id_gro`, `lvl`) VALUES
(38, '1.pdf', '2016-01-19', '08:01:48', 5, 1, 1),
(39, 'prez.etu.ul.codeIn24.pdf', '2016-03-20', '00:20:19', 5, 1, 2);

-- --------------------------------------------------------

--
-- Structure de la table `etat_compte`
--

DROP TABLE IF EXISTS `etat_compte`;
CREATE TABLE IF NOT EXISTS `etat_compte` (
  `id_etc` smallint(11) NOT NULL AUTO_INCREMENT,
  `ref_etc` text NOT NULL,
  `actif_etc` int(1) NOT NULL,
  `code_etc` text NOT NULL,
  PRIMARY KEY (`id_etc`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `etat_compte`
--

INSERT INTO `etat_compte` (`id_etc`, `ref_etc`, `actif_etc`, `code_etc`) VALUES
(8, '12345', 1, '69140261'),
(9, '12345', 0, '34961384');

-- --------------------------------------------------------

--
-- Structure de la table `favoris`
--

DROP TABLE IF EXISTS `favoris`;
CREATE TABLE IF NOT EXISTS `favoris` (
  `id_fav` smallint(11) NOT NULL AUTO_INCREMENT,
  `id_com` smallint(11) NOT NULL,
  `id_gro` smallint(11) NOT NULL,
  PRIMARY KEY (`id_fav`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `favoris`
--

INSERT INTO `favoris` (`id_fav`, `id_com`, `id_gro`) VALUES
(11, 5, 1);

-- --------------------------------------------------------

--
-- Structure de la table `groupe`
--

DROP TABLE IF EXISTS `groupe`;
CREATE TABLE IF NOT EXISTS `groupe` (
  `id_gro` smallint(11) NOT NULL AUTO_INCREMENT,
  `titre_cou` varchar(60) NOT NULL,
  `id_cat` int(11) NOT NULL,
  PRIMARY KEY (`id_gro`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `groupe`
--

INSERT INTO `groupe` (`id_gro`, `titre_cou`, `id_cat`) VALUES
(1, 'titre', 1),
(4, 'TEST', 2),
(5, 'TEST1', 1),
(6, 'orjfiekd', 1);

-- --------------------------------------------------------

--
-- Structure de la table `libelle_compte`
--

DROP TABLE IF EXISTS `libelle_compte`;
CREATE TABLE IF NOT EXISTS `libelle_compte` (
  `id_lic` int(11) NOT NULL AUTO_INCREMENT,
  `libelle_lic` varchar(60) NOT NULL,
  PRIMARY KEY (`id_lic`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `libelle_compte`
--

INSERT INTO `libelle_compte` (`id_lic`, `libelle_lic`) VALUES
(1, 'Etudiant'),
(2, 'Professeur'),
(3, 'Administrateur');

-- --------------------------------------------------------

--
-- Structure de la table `lvl_user`
--

DROP TABLE IF EXISTS `lvl_user`;
CREATE TABLE IF NOT EXISTS `lvl_user` (
  `id_lvl` smallint(6) NOT NULL AUTO_INCREMENT,
  `id_gro` int(11) NOT NULL,
  `id_com` int(11) NOT NULL,
  `nb_lvl` int(11) NOT NULL,
  PRIMARY KEY (`id_lvl`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `lvl_user`
--

INSERT INTO `lvl_user` (`id_lvl`, `id_gro`, `id_com`, `nb_lvl`) VALUES
(1, 1, 5, 2);

-- --------------------------------------------------------

--
-- Structure de la table `matiere`
--

DROP TABLE IF EXISTS `matiere`;
CREATE TABLE IF NOT EXISTS `matiere` (
  `id_mat` smallint(11) NOT NULL AUTO_INCREMENT,
  `libelle_mat` varchar(60) NOT NULL,
  PRIMARY KEY (`id_mat`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `matiere`
--

INSERT INTO `matiere` (`id_mat`, `libelle_mat`) VALUES
(1, 'Mathématiques'),
(2, 'Francais'),
(3, 'Histoire');

-- --------------------------------------------------------

--
-- Structure de la table `notes`
--

DROP TABLE IF EXISTS `notes`;
CREATE TABLE IF NOT EXISTS `notes` (
  `id_not` smallint(11) NOT NULL AUTO_INCREMENT,
  `resultat_not` int(11) NOT NULL,
  `id_com` smallint(11) NOT NULL,
  `id_cou` smallint(11) NOT NULL,
  PRIMARY KEY (`id_not`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `qcm`
--

DROP TABLE IF EXISTS `qcm`;
CREATE TABLE IF NOT EXISTS `qcm` (
  `id_qcm` smallint(11) NOT NULL AUTO_INCREMENT,
  `question_qcm` text NOT NULL,
  `num_qcm` int(11) NOT NULL,
  `id_cou` smallint(11) NOT NULL,
  PRIMARY KEY (`id_qcm`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `qcm`
--

INSERT INTO `qcm` (`id_qcm`, `question_qcm`, `num_qcm`, `id_cou`) VALUES
(4, 'Mais qu''elle est la bonne couleur ?', 1, 38),
(5, 'Taille de ton penis ?', 2, 38);

-- --------------------------------------------------------

--
-- Structure de la table `reponse`
--

DROP TABLE IF EXISTS `reponse`;
CREATE TABLE IF NOT EXISTS `reponse` (
  `id_rep` smallint(11) NOT NULL AUTO_INCREMENT,
  `libelle_rep` text NOT NULL,
  `etat_rep` int(1) NOT NULL,
  `id_qcm` smallint(11) NOT NULL,
  PRIMARY KEY (`id_rep`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `reponse`
--

INSERT INTO `reponse` (`id_rep`, `libelle_rep`, `etat_rep`, `id_qcm`) VALUES
(2, 'Bleu', 0, 4),
(3, 'Vert', 1, 4),
(4, 'Rouge', 1, 4),
(5, 'Petit', 1, 5),
(6, 'Moyen', 1, 5),
(7, 'Gros', 0, 5);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
