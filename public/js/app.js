$(document).foundation();

$(document).ready(function()
  {
  $('#qte_qcm').click(function(event)
    {
    createQuestionReponse($('#qte').val());
    });
  });

// function ecriture(base_id)
//   {
//   var container = $('<ul>').prop('id', 'tasks');
//   for (var task of TaskManager.tasks)
//     {
//     container.append(task.display());
//     }
//   $(base_id).empty();
//   $(base_id).append(container);
//   }

function createQuestionReponse(qte)
  {
  var row = $('<div>').prop('class', 'row');
  var form = $('<form>').prop('method', 'post');
  var button = $('<div>').prop('class', 'row')
    .append($('<div>').prop('class', 'large-12 medium-12 small-12 text-center columns')
    .append($('<input>').prop('class', 'button').prop('type', 'submit').prop('name', 'qcmok').prop('value', 'Valider')));
  for (var i = 0; i < qte; i++)
    {
    var que = $('<div>').prop('class', 'row')
    var qlabel= $('<div>').prop('class', 'large-3 medium-3 small-12 columns').append($('<span>').prop('class', 'prefix').text('Question')).prop('style', 'padding:0');
    var ilabel = $('<div>').prop('class', 'large-9 columns').append($('<input>').prop('type', 'text').prop('name', 'question['+i+']').prop('required', 'true')).prop('style', 'padding:0');
    que = que.append(qlabel).append(ilabel);
    var rep = $('<div>').prop('class', 'row')
    var rlabel = $('<div>').prop('class', 'large-12 medium-3 small-12 columns').append($('<span>').prop('style', 'margin-top:30px').prop('class', 'prefix').text('Réponse')).prop('style', 'padding:0');
    var rep1 = $('<div>').prop('class', 'large-4 columns').append($('<input>').prop('type', 'text').prop('name', 'reponse['+i+'][0]').prop('required', 'true'));
    var ind = $('<div>').prop('class', 'large-11 columns').append($('<p>').prop('style', 'text-align:center').prop('class', 'indice-left').text('La 1ère valeur est la bonne réponse')).prop('style', 'margin-top:10px');
    var rep2 = $('<div>').prop('class', 'large-4 columns').append($('<input>').prop('type', 'text').prop('name', 'reponse['+i+'][1]').prop('required', 'true'));
    var rep3 = $('<div>').prop('class', 'large-4 columns').append($('<input>').prop('type', 'text').prop('name', 'reponse['+i+'][2]').prop('required', 'true'));
    rep = rep.append(rlabel).append(ind).append(rep1).append(rep2).append(rep3);
    row.append(form.append(que).append(rep).append(button));
    }
  $('#affqcm').empty();
  $('#affqcm').append(row);
  }
