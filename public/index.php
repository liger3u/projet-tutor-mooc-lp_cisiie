<?php
ini_set('display_errors','on');
error_reporting(E_ALL);

require '../vendor/autoload.php';

use app\model\Matiere as Matiere;

date_default_timezone_set('Europe/Paris');

session_cache_limiter(false);
session_start();

$app = new \Slim\Slim(array(
  'view' => new \Slim\Views\Twig(),
  'templates.path' => '../app/templates'
));

// modification de l'environnement de slim : ajout des variables de session
$twig = $app->view->getEnvironment();
$twig->enableDebug();
$twig->addExtension(new Twig_Extension_Debug());
require '../app/routes.php';

$app->run();

?>
