<?php
/**
 * Ce fichier fait partie du projet MOOC.
 *
 * Ce projet à pour but la réalisation d'un MOOC
 * d'un point de vue étudiant
 *
 * @author Joffrey DALENCON
 * @author Luc LIGER
 * @author Nicolas PARADIS
 * @author Jeremy PETITCOLIN
 *
 * @package app/model
 * @copyright 2016 MOOC
 *
 * @version 1.5.5 - 24/03/2016
 */

namespace app\model;

use Illuminate\Database\Eloquent\Model as Model;

/**
 *  Model de la table Compte
 */
class Compte extends Model
{

    /*
     * Attributs
     */
    protected $table = 'compte'; // Nom table
    protected $primaryKey = 'id_etc'; // Clé primaire
    public $timestamps=false; // optionnel

    
    /*
     * Constructeur
     */
    public function __construct() {}

    
    /**
     * Récupère l'état en fonction du compte
     *
     * @return      Object      Etat 
     */
    public function Etat()
    {
        return $this->belongsTo('app\model\Etat_Compte', 'id_etc');
    }

    
    /**
     * Récupère le Libelle du compte en fonction du comtpe
     *
     * @return      Object      Libelle_Compte 
     */
    public function Libelle()
    {
        return $this->belongsTo('app\model\Libelle_Compte', 'id_lic');
    }
}


?>
