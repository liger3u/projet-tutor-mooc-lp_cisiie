<?php
/**
 * Ce fichier fait partie du projet MOOC.
 *
 * Ce projet à pour but la réalisation d'un MOOC
 * d'un point de vue étudiant
 *
 * @author Joffrey DALENCON
 * @author Luc LIGER
 * @author Nicolas PARADIS
 * @author Jeremy PETITCOLIN
 *
 * @package app/model
 * @copyright 2016 MOOC
 *
 * @version 1.5.5 - 24/03/2016
 */

namespace app\model;

use Illuminate\Database\Eloquent\Model as Model;

/**
 *  Model de la table Matiere
 */
class Matiere extends Model
{

    /*
     * Attributs
     */
    protected $table = 'matiere'; // Nom table
    protected $primaryKey = 'id_mat'; // Clé primaire
    public $timestamps=false; // optionnel

    
    /*
     * Constructeur
     */
    public function __construct() {}

    
    /**
     * Récupère les cours en fonction de la Matiere
     *
     * @return      Array       Tableau des cours 
     */
    public function Cours()
    {
        return $this->hasMany('app\model\Cours', 'id_cou');
    }
}

?>
