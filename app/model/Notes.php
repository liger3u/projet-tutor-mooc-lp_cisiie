<?php
/**
 * Ce fichier fait partie du projet MOOC.
 *
 * Ce projet à pour but la réalisation d'un MOOC
 * d'un point de vue étudiant
 *
 * @author Joffrey DALENCON
 * @author Luc LIGER
 * @author Nicolas PARADIS
 * @author Jeremy PETITCOLIN
 *
 * @package app/model
 * @copyright 2016 MOOC
 *
 * @version 1.5.5 - 24/03/2016
 */

namespace app\model;

use Illuminate\Database\Eloquent\Model as Model;

/**
 *  Model de la table Notes
 */
class Notes extends Model
{

    /*
     * Attributs
     */
    protected $table = 'notes'; // Nom table
    protected $primaryKey = 'id_not'; // Clé primaire
    public $timestamps=false; // optionnel

    
    /**
     * Constructeur
     */
    public function __construct() {}
    
    
    /**
     * Récupère le Compte en fonction des notes
     *
     * @return      Object      Compte
     */
    public function Compte()
    {
        return $this->belongsTo('app\model\Compte', 'id_com');
    }
    
    
    /**
     * Récupère le Cours en fonction des notes
     *
     * @return      Object      Cours
     */
    public function Cours()
    {
        return $this->belongsTo('app\model\Cours', 'id_cou');
    }
}


?>
