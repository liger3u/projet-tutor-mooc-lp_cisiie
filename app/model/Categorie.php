<?php
/**
 * Ce fichier fait partie du projet MOOC.
 *
 * Ce projet à pour but la réalisation d'un MOOC
 * d'un point de vue étudiant
 *
 * @author Joffrey DALENCON
 * @author Luc LIGER
 * @author Nicolas PARADIS
 * @author Jeremy PETITCOLIN
 *
 * @package app/model
 * @copyright 2016 MOOC
 *
 * @version 1.5.5 - 24/03/2016
 */

namespace app\model;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Model de la table Categorie
 */
class Categorie extends Model
{

    /*
     * Attributs
     */
    protected $table = 'categorie'; // Nom table
    protected $primaryKey = 'id_cat'; // Clé primaire
    public $timestamps=false; // optionnel

    
    /*
     * Constructeur
     */
    public function __construct() {}
    
    
    /**
     * Récupère la Matiere en fonction de la Categorie
     *
     * @return      Object      Matiere
     */
    public function Matiere()
    {
        return $this->belongsTo('app\model\Matiere', 'id_mat');
    }
}


?>
