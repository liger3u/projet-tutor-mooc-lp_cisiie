<?php
/**
 * Ce fichier fait partie du projet MOOC.
 *
 * Ce projet à pour but la réalisation d'un MOOC
 * d'un point de vue étudiant
 *
 * @author Joffrey DALENCON
 * @author Luc LIGER
 * @author Nicolas PARADIS
 * @author Jeremy PETITCOLIN
 *
 * @package app/model
 * @copyright 2016 MOOC
 *
 * @version 1.5.5 - 24/03/2016
 */

namespace app\model;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Model de la table Libelle_Compte
 */
class Libelle_Compte extends Model
{

    /*
     * Attributs
     */
    protected $table = 'libelle_compte'; // Nom table
    protected $primaryKey = 'id_lic'; // Clé primaire
    public $timestamps=false; // optionnel

    
    /*
     * Constructeur
     */
    public function __construct() {}

    
    /**
     * Récupère les comptes en fonction du Libelle_Compte
     *
     * @return      Array       Tableau de comptes 
     */
    public function comptes()
    {
        return $this->hasMany('app\model\Compte', 'id_com');
    }
}

?>
