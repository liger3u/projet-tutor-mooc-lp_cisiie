<?php
/**
 * Ce fichier fait partie du projet MOOC.
 *
 * Ce projet à pour but la réalisation d'un MOOC
 * d'un point de vue étudiant
 *
 * @author Joffrey DALENCON
 * @author Luc LIGER
 * @author Nicolas PARADIS
 * @author Jeremy PETITCOLIN
 *
 * @package app/model
 * @copyright 2016 MOOC
 *
 * @version 1.5.5 - 24/03/2016
 */

namespace app\model;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Model de la table Groupe
 */
class Groupe extends Model
{

    /*
     * Attributs
     */
    protected $table = 'groupe'; // Nom table
    protected $primaryKey = 'id_gro'; // Clé primaire
    public $timestamps=false; // optionnel

    
    /*
     * Constructeur
     */
    public function __construct() {}
    
    
    /**
     * Récupère la categorie en fonction du groupe
     *
     * @return      Object      Categorie 
     */
    public function Categorie()
    {
        return $this->belongsTo('app\model\Categorie', 'id_cat');
    }
}
?>
