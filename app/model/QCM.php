<?php
/**
 * Ce fichier fait partie du projet MOOC.
 *
 * Ce projet à pour but la réalisation d'un MOOC
 * d'un point de vue étudiant
 *
 * @author Joffrey DALENCON
 * @author Luc LIGER
 * @author Nicolas PARADIS
 * @author Jeremy PETITCOLIN
 *
 * @package app/model
 * @copyright 2016 MOOC
 *
 * @version 1.5.5 - 24/03/2016
 */

namespace app\model;

use Illuminate\Database\Eloquent\Model as Model;

/**
 *  Model de la table QCM
 */
class QCM extends Model
{

    /*
     * Attributs
     */
    protected $table = 'qcm'; // Nom table
    protected $primaryKey = 'id_qcm'; // Clé primaire
    public $timestamps=false; // optionnel

    
    /*
     * Constructeur
     */
    public function __construct() {}
    
    
    /**
     * Récupère le cours en fonction du QCm
     *
     * @return      Object      Cours 
     */
    public function Cours()
    {
        return $this->belongsTo('app\model\Cours', 'id_cou');
    }
    
    
    /**
     * Récupère le QCM en fonction de la réponse
     *
     * @return      Array       Tableau de questions
     */
    public function Questions()
    {
        return $this->hasMany('app\model\Question', 'id_que');
    }
}

?>
