<?php
/**
 * Ce fichier fait partie du projet MOOC.
 *
 * Ce projet à pour but la réalisation d'un MOOC
 * d'un point de vue étudiant
 *
 * @author Joffrey DALENCON
 * @author Luc LIGER
 * @author Nicolas PARADIS
 * @author Jeremy PETITCOLIN
 *
 * @package app/model
 * @copyright 2016 MOOC
 *
 * @version 1.5.5 - 24/03/2016
 */

namespace app\model;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Model de la table Favoris
 */
class Favoris extends Model
{

    /*
     * Attributs
     */
    protected $table = 'favoris'; // Nom table
    protected $primaryKey = 'id_fav'; // Clé primaire
    public $timestamps=false; // optionnel

    
    /*
     * Constructeur
     */
    public function __construct() {}

    
    /**
     * Récupère le compte en fonction du favoris
     *
     * @return      Object      Compte
     */
    public function Compte()
    {
        return $this->belongsTo('app\model\Compte', 'id_com');
    }

    
    /**
     * Récupère le cours en fonction du favoris
     *
     * @return      Object      Cours 
     */
    public function Cours()
    {
        return $this->belongsTo('app\model\Cours', 'id_cou');
    }
}


?>
