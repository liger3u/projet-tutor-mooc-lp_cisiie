<?php
/**
 * Ce fichier fait partie du projet MOOC.
 *
 * Ce projet à pour but la réalisation d'un MOOC
 * d'un point de vue étudiant
 *
 * @author Joffrey DALENCON
 * @author Luc LIGER
 * @author Nicolas PARADIS
 * @author Jeremy PETITCOLIN
 *
 * @package app/model
 * @copyright 2016 MOOC
 *
 * @version 1.5.5 - 24/03/2016
 */

namespace app\model;

use Illuminate\Database\Eloquent\Model as Model;

/**
 *  Model de la table Etat_Compte
 */
class Etat_Compte extends Model
{

    /*
     * Attributs
     */
    protected $table = 'etat_compte'; // Nom table
    protected $primaryKey = 'id_etc'; // Clé primaire
    public $timestamps=false; // optionnel

    
    /**
     * Constructeur
     */
    public function __construct() {}

    
    /**
     * Récupère les cours en fonction de l'état
     *
     * @return      Array       Tableau des comptes
     */
    public function comptes()
    {
        return $this->hasMany('app\model\Compte', 'id_com');
    }
}

?>
