<?php
/**
 * Ce fichier fait partie du projet MOOC.
 *
 * Ce projet à pour but la réalisation d'un MOOC
 * d'un point de vue étudiant
 *
 * @author Joffrey DALENCON
 * @author Luc LIGER
 * @author Nicolas PARADIS
 * @author Jeremy PETITCOLIN
 *
 * @package app/control
 * @copyright 2016 MOOC
 *
 * @version 1.5.5 - 24/03/2016
 */

use app\model\Compte as Compte;
use app\model\Etat_Compte as ECompte;
use app\model\Libelle_Compte as LCompte;

/* 
    Traitement pour la connexion au site 
*/
if(isset($_POST['conn']) && $_POST['conn'] == 'Connexion')
{
    // On récupère les informations du post
    $mail = $_POST['mail'];
    $pass = $_POST['pass'];
    $pass = trim($pass);
    $pass = htmlentities($pass);
    $request = Compte::whereMail_com($mail)->wherePass_com(md5($pass))->first();
    
    // On vérifie que le compte éxiste bien
    if(count(Compte::whereMail_com($mail)->first()) == 0)
    {
        $app->render('members/login.twig', array('mail_fail' => $mail, 'error' => 'Ce compte n\'éxiste pas, vérifiez la validité du mail.'));
    }
    // Si il éxiste bien, on vérifie que le mdp soit bon
    else if(count($request) == 0)
    {
        $app->render('members/login.twig', array('mail_fail' => $mail, 'error' => 'Mot de passe incorrect.'));
    }
    // Si il éxiste bien et si le mdp est bon, on vérifie que le comtpe est bien actif
    else if(ECompte::find($request->id_etc)->actif_etc == 0)
    {
        $app->render('members/verif.twig');
    }
    // Si toutes les conditions sont bonnes, on se connecte
    else
    {
        $_SESSION['id'] = $request->id_com;
        $_SESSION['nom'] = $request->nom_com;
        $_SESSION['pnom'] = $request->prenom_com;
        $_SESSION['etat'] = $request->libelle()->first()->libelle_lic;
        $app->redirect('./');
    }
}

/* 
    Traitement pour l'inscription au site 
*/
if(isset($_POST['ins']) && $_POST['ins'] == 'Inscription')
{
    // On récupère les informations du post 
    $nom = $_POST['nom'];
    $pnom = $_POST['pnom'];
    $mail = $_POST['mail'];
    $cmail = $_POST['cmail'];
    $pass = $_POST['pass'];
    $cpass = $_POST['cpass'];
    $type = $_POST['etat'];
    $ref = $_POST['ref'];

    // On test que les 2 mails correspondent
    if($mail != $cmail)
        $app->render('members/signin.twig', array(
            'nom_fail' => $nom,
            'pnom_fail' => $pnom,
            'mail_fail' => $mail,
            'cmail_fail' => $cmail,
            'ref_fail' => $ref,
            'error' => 'Les deux emails ne correspondent pas.'
        ));
    // On test que les 2 mdp correspondent
    else if($pass != $cpass)
        $app->render('members/signin.twig', array(
            'nom_fail' => $nom,
            'pnom_fail' => $pnom,
            'mail_fail' => $mail,
            'cmail_fail' => $cmail,
            'ref_fail' => $ref,
            'error' => 'Les deux password ne correspondent pas.'
        ));
    // On test que le compte n'éxiste pas
    else if(count(Compte::whereMail_com($mail)->first()) == 1)
        $app->render('members/signin.twig', array(
            'nom_fail' => $nom,
            'pnom_fail' => $pnom,
            'mail_fail' => $mail,
            'cmail_fail' => $cmail,
            'ref_fail' => $ref,
            'error' => 'Il éxiste déjà un compte avec cet email.'
        ));
    // Si les conditions sont bonnes, on éffectue l'inscription
    else
    {
        $id_lic = LCompte::whereLibelle_lic($type)->first();

        // On créé un code random pour la vérifition de l'adresse mail et l'activation du compte
        $code_val = rand('11111111', '99999999');
        $ec = new ECompte();
        $ec->ref_etc = $ref;
        $ec->actif_etc = 0;
        $ec->code_etc = $code_val;
        $ec->push();
        
        // On créé le compte
        $c = new Compte();
        $c->nom_com = $nom;
        $c->prenom_com = $pnom;
        $c->mail_com = $mail;
        $c->pass_com = md5($pass);
        $c->adresse_com = 'Non renseigne';
        $c->cp_com = 0000;
        $c->ville_com = 'Non renseigne';
        $c->dateNaiss_com = '0000-00-00';
        $c->dateCreat_com = date('Y-m-d');
        $c->id_etc = $ec->id_etc;
        $c->id_lic = $id_lic->id_lic;
        $c->push();
        
        // On envoie le mail d'activiation de compte
        mail_envoieValCompte($c->mail_com, $ec->code_etc, $c->prenom_com.' '.$c->nom_com);

        $app->redirect('verif_compte/'.$c->mail_com);
    }
}

/* 
    Traitement de vérification du code d'activation 
*/
if(isset($_POST['valider']) && $_POST['valider'] == "Valider")
{
    // On récupère le code insérer et le bon code
    $c = Compte::whereMail_com($_POST['mail'])->first();
    $ec = ECompte::find($c->id_etc);
    
    // On vérifie que les 2 codes correspondent
    if($_POST['code'] == $ec->code_etc)
    {
        $ec->actif_etc = 1;
        $ec->save();

        $_SESSION['id'] = $c->id_com;
        $_SESSION['nom'] = $c->nom_com;
        $_SESSION['pnom'] = $c->prenom_com;
        $_SESSION['etat'] = $c->libelle()->first()->libelle_lic;
        mail_envoieValOk($c->mail_com, $c->prenom_com.' '.$c->nom_com);
        $app->redirect("../user/$c->id_com");
    }
    else
        $app->render('members/verif.twig', array('mail' => $_POST['mail']));
}
?>
