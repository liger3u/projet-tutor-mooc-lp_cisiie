<?php

/**
 * Ce fichier fait partie du projet MOOC.
 *
 * Ce projet à pour but la réalisation d'un MOOC
 * d'un point de vue étudiant
 *
 * @author Joffrey DALENCON
 * @author Luc LIGER
 * @author Nicolas PARADIS
 * @author Jeremy PETITCOLIN
 *
 * @package app/control
 * @copyright 2016 MOOC
 *
 * @version 1.5.5 - 24/03/2016
 */

use app\model\Matiere as Matiere;
use app\model\Cours as Cours;
use app\model\QCM as QCM;
use app\model\Reponse as Reponse;
use app\model\Question as Question;
use app\model\Notes as Notes;
use app\model\Lvl_user as Lvl;

// variable permettant les calculs
$i = 0;
$rep = array();
$faute = array();
$reponse = array();
$test;
$point = 0;
$total = 0;

// On récupère le tableau des bonnes réponses
foreach ($_POST as $key => $value)
{
    $rep[$i]['question'] = $key;
    $rep[$i]['reponse'] = $value;
    $total++;
    $reponse[$i] = Reponse::where('id_qcm', '=', $key)->get();
    foreach ($reponse[$i] as $key => $value) 
    {
        if ($value['etat_rep'] == '0')
        {
            $test = $value['libelle_rep'];
        }
    }
    // Si c'est la bonne réponse, on ajoute 1 point
    if($rep[$i]['reponse']==$test)
    {
        $point++;
    }
    else
    {
        $faute[$i]['num']= QCM::find($rep[$i]['question'])->num_qcm;
        $faute[$i]['intitule']= QCM::find($rep[$i]['question'])->question_qcm;
        $faute[$i]['rep'] = $rep[$i]['reponse'];
    }
    $i++;
}

// On ajoute le résultat dans la table note
$n = new Notes();
$n->resultat_not = $point;
$n->id_com = $_SESSION["id"];
$n->id_cou = $id_cou;
$n->push();


$id_gro = Cours::find($id_cou)->id_gro;

// Si il a obtenu la moyenne
// on augmente son lvl pour passer au chapitre suivant
if($total/2 < $point)
{   
    if(Lvl::whereId_gro($id_gro)->whereId_com($_SESSION['id'])->first() != NULL)
    {
        if(Lvl::whereId_gro($id_gro)->whereId_com($_SESSION['id'])->first()->nb_lvl < Cours::find($id_cou)->lvl)
        {
            $lvl = Lvl::whereId_gro($id_gro)->whereId_com($_SESSION['id'])->first();
            $lvl->nb_lvl += 1;
            $lvl->save();   
        }
    }
    else 
    {
        $lvl = new Lvl();
        $lvl->id_gro = $id_gro;
        $lvl->id_com = $_SESSION["id"];
        $lvl->nb_lvl = 1;
    }
}

$app->render('qcm/noteqcm.twig', array(
    'point' => $point,
    'total' => $total,
    'faute' => $faute,
    'idgro' => $id_gro
));
?>
