<?php
/**
 * Ce fichier fait partie du projet MOOC.
 *
 * Ce projet à pour but la réalisation d'un MOOC
 * d'un point de vue étudiant
 *
 * @author Joffrey DALENCON
 * @author Luc LIGER
 * @author Nicolas PARADIS
 * @author Jeremy PETITCOLIN
 *
 * @package app/control
 * @copyright 2016 MOOC
 *
 * @version 1.5.5 - 24/03/2016
 */

use app\model\Categorie as Cat;
use app\model\Matiere as Mat;

/*
    On reçoit le nom de la matiere
    On récupère la matière
    On reforme notre select avec les categories liées à la matière
*/
$m = Mat::whereLibelle_mat($_POST["mat"])->first();
echo "<select name='categorie'>";
foreach(Cat::whereId_mat($m->id_mat)->get() as $key => $value)
    echo "<option value='".$key."'>".$key."</option>";
echo "</select>";
?>