<?php
/**
 * Ce fichier fait partie du projet MOOC.
 *
 * Ce projet à pour but la réalisation d'un MOOC
 * d'un point de vue étudiant
 *
 * @author Joffrey DALENCON
 * @author Luc LIGER
 * @author Nicolas PARADIS
 * @author Jeremy PETITCOLIN
 *
 * @package app/control
 * @copyright 2016 MOOC
 *
 * @version 1.5.5 - 24/03/2016
 */

use app\model\Matiere as Matiere;
use app\model\Cours as Cours;
use app\model\QCM as QCM;
use app\model\Reponse as Reponse;
use app\model\Groupe as Groupe;
use app\model\Categorie as Cat;
use app\model\Lvl_user as Lvl;

if(isset($_POST["go"]) && $_POST['go'] == 'Valider et passer à la creation des cours')
{
    if(Count(Groupe::whereTitre_cou($_POST["titre"])->get()) == 0)
    {
        $g = new Groupe;
        $g->titre_cou = $_POST["titre"];
        $g->id_cat = Cat::whereLibelle_cat($_POST["categorie"])->first()->id_cat;
        $g->lvl = 0;
        $g->push();
        $app->render('cours/create.twig', array('id_groupe' => $g->id_gro));
    }
    else $app->render('cours/index.twig', array('matieres' => Matiere::orderBy('libelle_mat')->get(), 'error' => 'Ce titre existe déjà !'));
}

if(isset($_POST['goqcm']) && $_POST['goqcm'] == 'Valider et passer au qcm')
{
    $error = FALSE;

    if(isset($_POST['lien']) && $_POST['lien'] != "")
        $lien_video= str_replace('watch?v=','v/',$_POST['lien']);
    else $lien_video = "no video";

    $lvl = 1;

    $id_groupe = $_POST['id_groupe'];

    $lvl_ex = Groupe::whereid_gro($id_groupe)->first();

    if(isset($lvl_ex)){
        $lvl = $lvl_ex->lvl+1;
    }
    else $lvl_ex = new Groupe();

    $lvl_ex->lvl = $lvl;
    $lvl_ex->save();


    $pdf = $_FILES['cours'];
    $dossier = '../public/cours/';
    $fichier = basename($pdf['name']);
    $extensions = array('.pdf');
    $extension = strrchr($pdf['name'], '.');
    $fichier = strtr($fichier, 'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ', 'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
    $fichier = preg_replace('/([^.a-z0-9]+)/i', '-', $fichier);

    if(!in_array($extension, $extensions))
    {
        $error = TRUE;
        $app->render('cours/create.twig', array(
            'titre_fail' => $lvl_ex->titre_cou,
            'error' => 'Vous devez uploader un fichier de type pdf.',
            'matieres' => Matiere::orderBy('libelle_mat')->get()
        ));
    }

    else if(Count(Cours::wherePdf_cou($fichier.'.pdf')->get()) != 0)
    {
        $error = TRUE;
        $app->render('cours/create.twig', array(
            'titre_fail' => $lvl_ex->titre_cou,
            'error' => 'Le nom du fichier éxiste déjà, veuillez renommer le fichier.',
            'matieres' => Matiere::orderBy('libelle_mat')->get()
        ));
    }

    if(!$error)
    {
        if(move_uploaded_file($pdf['tmp_name'], $dossier . $fichier))
        {
            $c = new Cours();
            $c->pdf_cou = $fichier;
            $c->lien_video = $lien_video;
            $c->datePublie_cou = date('Y-m-d');
            $c->heurePublie_cou = date('h:m:s');
            $c->id_com = $_SESSION['id'];
            $c->id_gro = $id_groupe;
            $c->lvl = $lvl;
            $c->push();
            
            $l = new Lvl();
            $l->id_gro = $id_groupe;
            $l->id_com = $_SESSION['id'];
            $l->nb_lvl = 1;
            $l->push();
            
            $app->redirect('../qcm/create/'.$c->id_cou);
        }
        else
        {
            $app->render('cours/create.twig', array(
                'titre_fail' => $lvl_ex->titre_cou,
                'error' => 'Votre fichier est trop lourd !',
                'matieres' => Matiere::orderBy('libelle_mat')->get()
            ));
        }
    }
}


if(isset($_POST['qcmok']) && $_POST['qcmok'] == "Valider")
{
    $que = $_POST['question'];
    $rep = $_POST['reponse'];

    for ($i=0; $i < sizeof($que); $i++)
    {
        $q = new QCM();
        $q->question_qcm = $que[$i];
        $q->num_qcm = $i+1;
        $q->id_cou = $id;
        $q->push();

        for($j=0; $j < sizeof($rep[$i]); $j++)
        {
            $r = new Reponse();
            $r->libelle_rep = $rep[$i][$j];
            if($j == 0)
                $r->etat_rep = 0;
            else $r->etat_rep = 1;
            $r->id_qcm = $q->id_qcm;
            $r->push();
        }
    }
    $idg = Cours::whereId_cou($id)->first()->id_gro;
    $app->redirect("../../cours/$idg");
}

?>
