<?php
use app\model\Admin as Admin;
use app\model\Compte as Compte;
use app\model\Groupe as Groupe;
use app\model\Etat_Compte as ECompte;

if(isset($_POST['modif']))
{
    $c = Compte::whereId_com($id)->first();
    $c->nom_com = $_POST['nom'];
    $c->prenom_com = $_POST['pnom'];
    $c->mail_com = $_POST['mail'];
    $c->adresse_com = $_POST['adr'];
    $c->cp_com = $_POST['cp'];
    $c->ville_com = $_POST['vil'];
    $c->dateNaiss_com = $_POST['date'];
    $c->save();

    $app->redirect('../../../adm');
}

if(isset($_POST['add']) && $_POST['add'] == "Ajouter")
{
    $c = Compte::whereId_com($_POST["addAdm"])->first();
    if($c != null)
    {
        $a = new Admin;
        $a->id_com = $_POST["addAdm"];
        $a->droit_adm = 1;
        $a->push();
        $app->redirect('../adm');
    }
}

if(isset($_POST['supp']) && $_POST['supp'] == "Supprimer")
{
    $c = Compte::whereId_com($_POST["suppAdm"])->first();
    if($c != null)
    {
        $a = Admin::whereId_com($_POST["suppAdm"])->first();
        $a->delete();
        $app->redirect('../adm');
    }
}