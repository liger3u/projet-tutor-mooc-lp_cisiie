<?php
/**
 * Ce fichier fait partie du projet MOOC.
 *
 * Ce projet à pour but la réalisation d'un MOOC
 * d'un point de vue étudiant
 *
 * @author Joffrey DALENCON
 * @author Luc LIGER
 * @author Nicolas PARADIS
 * @author Jeremy PETITCOLIN
 *
 * @package app/control
 * @copyright 2016 MOOC
 *
 * @version 1.5.5 - 24/03/2016
 */

use app\model\Matiere as Matiere;

/*
    Traitement pour la création d'une matière et insertion dans la base
*/
if(isset($_POST['creer']) && $_POST['creer'] == 'Creer matiere')
{
    // variable permettant d'indiquer les erreurs occasionnées
    // FALSE si il n'y a pas d'erreur
    $error = FALSE;
    
    // On récupère les informations du post
    $titre = $_POST['titre'];
    
    // On test si la catégorie éxiste déjà
    if( Matiere::whereLibelle_mat($titre)->first())
    {
        {
            $error = TRUE;
            $app->render('matiere/create.twig', array(
                'titre_fail' => $titre,
                'error' => 'Cette matiere existe déjà.'
            ));
        }
    }
    // Sinon on la créer
    else
    {
        $m = new Matiere();
        $m->libelle_mat = $titre;
        $m->push();
        $app->redirect("../../public/matiere/$titre");
    }
}
?>
