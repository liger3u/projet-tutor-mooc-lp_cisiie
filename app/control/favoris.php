<?php
/**
 * Ce fichier fait partie du projet MOOC.
 *
 * Ce projet à pour but la réalisation d'un MOOC
 * d'un point de vue étudiant
 *
 * @author Joffrey DALENCON
 * @author Luc LIGER
 * @author Nicolas PARADIS
 * @author Jeremy PETITCOLIN
 *
 * @package app/control
 * @copyright 2016 MOOC
 *
 * @version 1.5.5 - 24/03/2016
 */

use app\model\Favoris as Fav;

/* 
    On récupère l'action demandées
    En fonction de l'action demandée, on appelle la fonction
     --> addFav : permet l'ajout du cours en favoris
     --> suppFav : permet la suppression du cours en favoris
*/
switch($_GET['action'])
{
    case "add":
        addFav($_GET["idu"], $_GET["idc"]);
        break;
    case "del":
        suppFav($_GET["idu"], $_GET["idc"]);
        break;
}

/**
 * Permet l'ajout du cours dans les favoris
 *
 * @param       integer     $idu        id de l'utilisateur
 * @param       integer     $idc        id du cours
 */
function addFav($idu, $idc)
{
    $f = new Fav();
    $f->id_com = $idu;
    $f->id_cou = $idc;
    $f->push();
}

/**
 * Permet la suppression du cours dans les favoris
 *
 * @param       integer     $idu        id de l'utilisateur
 * @param       integer     $idc        id du cours
 */
function suppFav($idu, $idc)
{
    $f = Fav::whereId_com($idu)->Id_cou($idc)->first();
    $f->delete();
}
?>