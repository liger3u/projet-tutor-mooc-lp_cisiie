<?php
/**
 * Ce fichier fait partie du projet MOOC.
 *
 * Ce projet à pour but la réalisation d'un MOOC
 * d'un point de vue étudiant
 *
 * @author Joffrey DALENCON
 * @author Luc LIGER
 * @author Nicolas PARADIS
 * @author Jeremy PETITCOLIN
 *
 * @package app/control
 * @copyright 2016 MOOC
 *
 * @version 1.5.5 - 24/03/2016
 */

use app\model\Admin as Admin;
use app\model\Compte as Compte;
use app\model\Libelle_Compte as LCompte;

/**
 * Permet la verification que l'utilisateur est bien connecté
 *
 * @return       boolean     | TRUE     L'utilisateur est connecté
 *                           | FALSE    L'utilisateur n'est pas connecté
 */
function verif_Connect()
{
    if(isset($_SESSION["id"]))
        return true;
    else return false;
}

/**
 * Permet la verification des droits d'accès à la page
 *
 * @return       boolean     | TRUE     L'utilisateur a les droits
 *                           | FALSE    L'utilisateur n'a pas les droits
 */
function verif_EtatCompte($value)
{
    $c = Compte::whereId_com($_SESSION["id"])->first();
    $id = LCompte::whereLibelle_lic($value)->first()->id_lic;
    if($c->id_lic == $id)
        return true;
    else return false;
}

/**
 * Permet la verification des droits d'administration
 *
 * @return       boolean     | TRUE     L'utilisateur a les droits
 *                           | FALSE    L'utilisateur n'a pas les droits
 */
function verif_Admin()
{
    $a = Admin::whereId_com($_SESSION["id"])->first();
    if($a == null)
        return false;
    else return true;
}

/**
 * Permet l'envoi de mail de validation de compte
 *
 * @param       $destinataire       string      Mail de l'utilisateur
 * @param       $code_val           integer     Code de validation
 * @param       $nom                string      Nom de l'utilisateur
 */
function mail_envoieValCompte($destinataire, $code_val, $nom)
{   
    // Adresse de notre serveur mail
    $source = 'support@mooc.890m.com';
    
    // On filtre les serveurs
    if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $destinataire)) 
    {
        $passage_ligne = "\r\n";
    }
    else
    {
        $passage_ligne = "\n";
    }
    
    // Déclaration des messages au format texte et au format HTML.
    $message_txt = "Bonjour $nom,".$passage_ligne."Le code de validation est : $code_val.";
    $message_html = "<html><head></head><body>Bonjour $nom,<br/>Le code de validation est : $code_val.</body></html>";

    // Création de la boundary
    $boundary = "-----=".md5(rand());

    // Définition du sujet.
    $sujet = "[SUPPORT MOOC] - Code d'activation de votre compte !";

    // Création du message.
    $message = $passage_ligne."--".$boundary.$passage_ligne;
    
    // Ajout du message au format texte.
    $message.= "Content-Type: text/plain; charset=\"ISO-8859-1\"".$passage_ligne;
    $message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
    $message.= $passage_ligne.$message_txt.$passage_ligne;
    $message.= $passage_ligne."--".$boundary.$passage_ligne;
    
    // Ajout du message au format HTML
    $message.= "Content-Type: text/html; charset=\"ISO-8859-1\"".$passage_ligne;
    $message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
    $message.= $passage_ligne.$message_html.$passage_ligne;
    $message.= $passage_ligne."--".$boundary."--".$passage_ligne;
    $message.= $passage_ligne."--".$boundary."--".$passage_ligne;

    // Création du header de l'e-mail.
    $header = "From: \"Support Mooc\"<$source>".$passage_ligne;
    $header.= "Reply-to: \"Support Mooc\"<$source>".$passage_ligne;
    $header.= "MIME-Version: 1.0".$passage_ligne;
    $header.= "Content-Type: multipart/alternative;".$passage_ligne." boundary=\"$boundary\"".$passage_ligne;

    // Envoi de l'e-mail.
    mail($destinataire,$sujet,$message,$header);
}

/**
 * Permet l'envoi de mail de confirmation de validation de compte
 *
 * @param       $destinataire       string      Mail de l'utilisateur
 * @param       $nom                string      Nom de l'utilisateur
 */
function mail_envoieValOk($destinataire, $nom)
{
    // Adresse de notre serveur mail
    $source = 'support@mooc.890m.com';
    
    // On filtre les serveurs
    if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $destinataire)) 
    {
        $passage_ligne = "\r\n";
    }
    else
    {
        $passage_ligne = "\n";
    }
    
    // Déclaration des messages au format texte et au format HTML.
    $message_txt = "Bonjour $nom,".$passage_ligne."Votre compte à bien été activé.";
    $message_html = "<html><head></head><body>Bonjour $nom,<br/>Votre compte à bien été activél.</body></html>";

    // Création de la boundary
    $boundary = "-----=".md5(rand());

    // Définition du sujet.
    $sujet = "[SUPPORT MOOC] - Compte Activé !";

    //Création du message.
    $message = $passage_ligne."--".$boundary.$passage_ligne;
    
    // Ajout du message au format texte.
    $message.= "Content-Type: text/plain; charset=\"ISO-8859-1\"".$passage_ligne;
    $message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
    $message.= $passage_ligne.$message_txt.$passage_ligne;

    $message.= $passage_ligne."--".$boundary.$passage_ligne;
    // Ajout du message au format HTML
    $message.= "Content-Type: text/html; charset=\"ISO-8859-1\"".$passage_ligne;
    $message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
    $message.= $passage_ligne.$message_html.$passage_ligne;
    $message.= $passage_ligne."--".$boundary."--".$passage_ligne;
    $message.= $passage_ligne."--".$boundary."--".$passage_ligne;

    // Création du header de l'e-mail.
    $header = "From: \"Support Mooc\"<$source>".$passage_ligne;
    $header.= "Reply-to: \"Support Mooc\"<$source>".$passage_ligne;
    $header.= "MIME-Version: 1.0".$passage_ligne;
    $header.= "Content-Type: multipart/alternative;".$passage_ligne." boundary=\"$boundary\"".$passage_ligne;

    // Envoi de l'e-mail.
    mail($destinataire,$sujet,$message,$header);
}
?>
