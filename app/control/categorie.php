<?php
/**
 * Ce fichier fait partie du projet MOOC.
 *
 * Ce projet à pour but la réalisation d'un MOOC
 * d'un point de vue étudiant
 *
 * @author Joffrey DALENCON
 * @author Luc LIGER
 * @author Nicolas PARADIS
 * @author Jeremy PETITCOLIN
 *
 * @package app/control
 * @copyright 2016 MOOC
 *
 * @version 1.5.5 - 24/03/2016
 */

use app\model\Categorie as Cat;
use app\model\Matiere as Matiere;

/*
    Traitement pour la création d'une catégorie et insertion dans la base
*/
if(isset($_POST['creer']) && $_POST['creer'] == 'Creer categorie')
{
    // variable permettant d'indiquer les erreurs occasionnées
    // FALSE si il n'y a pas d'erreur
    $error = FALSE;
    
    // On récupère les informations du post
    $titre = $_POST['titre'];
    $mat = $_POST['matiere'];
    
    // On test si la catégorie éxiste déjà
    if(Cat::whereLibelle_cat($titre)->first())
    {
        {
            $error = TRUE;
            $app->render('categorie/create.twig', array(
                'titre_fail' => $titre,
                'error' => 'Cette categorie existe déjà.'
            ));
        }
    }
    // Sinon on la créer
    else
    {
        $c = new Cat();
        $c->libelle_cat = $titre;
        $c->id_mat = Matiere::whereLibelle_mat($mat)->first()->id_mat;
        $c->push();
        $app->redirect("../../public/matiere/$mat#$titre");
    }
}
?>
