<?php

/**
 * Ce fichier fait partie du projet MOOC.
 *
 * Ce projet à pour but la réalisation d'un MOOC
 * d'un point de vue étudiant
 *
 * @author Joffrey DALENCON
 * @author Luc LIGER
 * @author Nicolas PARADIS
 * @author Jeremy PETITCOLIN
 *
 * @package app
 * @copyright 2016 MOOC
 *
 * @version 1.5.5 - 24/03/2016
 */

use app\model\Admin as Admin;
use app\model\Compte as Compte;
use app\model\Etat_Compte as ECompte;
use app\model\Libelle_Compte as LCompte;
use app\model\Matiere as Matiere;
use app\model\Cours as Cours;
use app\model\QCM as QCM;
use app\model\Reponse as Rep;
use app\model\Categorie as Cat;
use app\model\Favoris as Fav;
use app\model\Groupe as Groupe;
use app\model\Lvl_user as Lvl;
use app\model\Notes as Notes;

// Include fichier contenant des fonctions utiles 
include 'control/function.php';


//////////////////////////////////////////////////////////////////////////////////////////
//                                                                      ENVIRONNEMENT   //
//////////////////////////////////////////////////////////////////////////////////////////

if (isset($_SESSION['id']))
{
    $twig->addGlobal('session',$_SESSION);
    $twig->addGlobal('favoris', Fav::whereId_com($_SESSION["id"])->get());
    $a = Admin::whereId_com($_SESSION["id"])->first();
    if($a == null)
        $twig->addGlobal('is_admin', 0);
    else $twig->addGlobal('is_admin', $a->droit_adm);
}
$twig->addGlobal('matiere', Matiere::orderBy("libelle_mat")->get());
$twig->addGlobal('groupe_fav', Groupe::orderBy("titre_cou")->get());


//////////////////////////////////////////////////////////////////////////////////////////
//                                                                              INDEX   //
//////////////////////////////////////////////////////////////////////////////////////////

$app->get('/', function () use ($app)
          {
              $app->render('index.twig', array('cours' => Groupe::orderBy('id_gro', 'desc')->take(4)->get()));
          });


//////////////////////////////////////////////////////////////////////////////////////////
//                                                                             COMPTE   //
//////////////////////////////////////////////////////////////////////////////////////////

/* 
    INSCRIPTION - Methode GET
*/
$app->get('/signin', function () use ($app)
          {
              if(isset($_SESSION["id"]))
              {
                  session_destroy();
                  $app->redirect("./signin");
              }
              $app->render('members/signin.twig');
          });

/* 
    INSCRIPTION - Methode POST
*/
$app->post('/signin', function () use ($app)
           {
               require_once 'control/compte.php';
           });

##############################################################################

/* 
    VERIFCATION - Methode GET
*/
$app->get('/verif_compte/:mail', function ($mail) use ($app)
          {
              $app->render('members/verif.twig', array('mail' => $mail));
          });

/* 
    VERIFCATION - Methode POST
*/
$app->post('/verif_compte/:mail', function ($mail) use ($app)
           {
               require_once 'control/compte.php';
           });

##############################################################################

/* 
    CONNEXION - Methode GET
*/
$app->get('/login', function () use ($app)
          {
              if(isset($_SESSION["id"]))
              {
                  session_destroy();
                  $app->redirect("./login");
              }
              $app->render('members/login.twig');
          });

/* 
    CONNEXION - Methode POST
*/
$app->post('/login', function () use ($app)
           {
               require_once 'control/compte.php';
           });

##############################################################################

/* 
    DECONNEXION
*/
$app->get('/logout', function () use ($app)
          {
              $_SESSION = array();
              session_destroy();
              $app->redirect('./');
          });

##############################################################################

/* 
    AFFICHAGE DE LA PAGE DE PROFIL 
*/
$app->get('/user/:id', function ($id) use ($app)
          {
              if($_SESSION["id"] != $id)
                  $app->redirect($_SESSION["id"]);
              if(verif_Connect() == false)
                  $app->redirect('../login');
              $c = Compte::whereId_com($id)->first();
              $app->render('members/profil.twig', array(
                  'compte' => $c,
                  'libelle' => $c->libelle()->first()->libelle_lic,
                  'etat' => $c->etat()->first()
              ));
          });

##############################################################################

/* 
    PASSAGE DE PROF A ETUDIANT ET INVERSEMENT 
*/
$app->get('/user/:id/status/:value', function ($id, $value) use ($app)
          {
              if(verif_Connect() == false)
                  $app->redirect('../login');
              $c = Compte::whereId_com($id)->first();
              if($value == "Etudiant")
              {
                  $c->id_lic = 1;
                  $c->save();
                  $_SESSION["etat"] = "Etudiant";
                  $app->redirect('../../../');
              }
              else if($value == "Professeur")
              {
                  $c->id_lic = 2;
                  $c->save();
                  $_SESSION["etat"] = "Professeur";
                  $app->redirect('../../../');
              }
          });

##############################################################################

/* 
    AFFICHAGE DE LA LISTE DES COURS CREER PAR LE PROF 
*/
$app->get('/user/:id/mescours', function ($id) use ($app)
          {
              if($_SESSION["id"] != $id)
                  $app->redirect("../".$_SESSION["id"]."/mescours");
              if(verif_Connect() == false)
                  $app->redirect('../login');
              if(verif_EtatCompte("Professeur"))
              {
                  $c = Cours::whereId_com($id)->get();
                  $id_taff = $groupe = array();
                  foreach($c as $key => $value)
                  {
                      if(!in_array($value->id_gro, $id_taff))
                      {
                          $id_taff[] = $value->id_gro;
                          $ind = sizeof($groupe);
                          $groupe[$ind]["id"] = $value->id_gro;
                          $groupe[$ind]["titre"] = Groupe::whereId_gro($value->id_gro)->first()->titre_cou;
                      }
                  }
                  $app->render('cours/mescours.twig', array("listcours" => $groupe));
              }
              else $app->redirect("../");
          });

##############################################################################

/* 
    MODIFICATION DU PROFIL - Methode GET 
*/
$app->get('/user/:id/modif', function($id) use ($app)
          {
              if($_SESSION["id"] != $id)
                  $app->redirect("../"+$_SESSION["id"]."modif");
              $app->render('members/modif_profil.twig', array("compte" => Compte::whereId_com($id)->first()));
          });

/* 
    MODIFICATION DU PROFIL - Methode POST 
*/
$app->post('/user/:id/modif', function($id) use ($app)
           {
               $pro = Compte::whereId_com($id)->first();
               $pro->adresse_com = $_POST['adr'];
               $pro->cp_com = $_POST['cp'];
               $pro->ville_com = $_POST['vil'];
               $pro->dateNaiss_com = date('Y-m-d', strtotime($_POST['nais']));
               $pro->save();
               $app->redirect('../../user/'.$id);
           });


//////////////////////////////////////////////////////////////////////////////////////////
//                                                                              COURS   //
//////////////////////////////////////////////////////////////////////////////////////////

/* 
    CREATION D'UN COURS - Methode GET 
*/
$app->get('/cours/create', function () use ($app)
          {
              if(verif_Connect() == false)
                  $app->redirect('../login');
              if(isset($_SESSION['id']) && $_SESSION['etat'] != 'Etudiant' )
                  $app->render('cours/index.twig', array('matieres' => Matiere::orderBy('libelle_mat')->get()));
              else $app->redirect('../');
          });

/* 
    CREATION D'UN COURS - Methode POST 
*/
$app->post('/cours/create', function () use ($app)
           {
               require_once 'control/cours.php';
           });

##############################################################################

/*
    CONTINUER UN COURS - Methode GET
*/
$app->get('/cours/ajout', function () use ($app)
          {
              if(verif_Connect() == false)
                  $app->redirect('../login');
              if(isset($_SESSION['id']) && $_SESSION['etat'] != 'Etudiant' )
                  $app->render('cours/ajout.twig', array('cours' => Groupe::orderBy('titre_cou')->get()));
              else $app->redirect('../');
          });

/*
    CONTINUER UN COURS - Methode POST
*/
$app->post('/cours/ajout', function () use ($app)
           {
               require_once 'control/cours.php';
           });

##############################################################################

/* 
    AFFICHAGE D'UN COURS
*/
$app->get('/cours/:id', function ($id) use ($app)
          {
              if(verif_Connect() == false)
                  $app->redirect('../login');

              // Permet de masquer le button en fonction du boolean
              $butt_suiv = false;
              $butt_prec = false;

              $qcm = array();
              if(Lvl::whereId_com($_SESSION["id"])->whereId_gro($id)->first() == null)
              {
                  $l = new Lvl();
                  $l->id_com = $_SESSION["id"];
                  $l->id_gro = $id;
                  $l->nb_lvl = 1;
                  $l->push();
              }
              $lvl = Lvl::whereId_com($_SESSION["id"])->whereId_gro($id)->first();
              $cours = Cours::whereId_gro($id)->whereLvl($lvl->nb_lvl)->first();

              $i = 0;

              // Si on cherche le 1er chapitre
              // On masque le bouton "suivant"
              if(1 == $cours->lvl)
              {
                  $butt_prec = true;
              }

              // Si le chapitre cherché est le dernier chapitre débloqué
              // On masque le bouton "suivant"
              else if(Lvl::whereId_com($_SESSION["id"])->whereId_gro($id)->first()->nb_lvl == $cours->lvl)
              {
                  $butt_suiv = true;
              }

              if($cours != NULL)
              {
                  foreach(QCM::whereId_cou($cours->id_cou)->get() as $key)
                  {
                      $qcm[$i]['question'] = $key->question_qcm;
                      $qcm[$i]['numrep'] = $key->id_qcm;
                      $qcm[$i]['numquestion'] = $key->num_qcm;
                      $ind = 0;

                      $tab_res = Rep::whereId_qcm($key->id_qcm)->get();
                      $tab_qcm[0] = ['1','2','3'];
                      $tab_qcm[1] = ['2','1','3'];
                      $tab_qcm[2] = ['1','3','2'];
                      $tab_qcm[3] = ['3','2','1'];
                      $tab_qcm[4] = ['3','1','2'];

                      $rand = mt_rand(0,4);
                      foreach($tab_qcm[$rand] as $key)
                      {
                          $qcm[$i]['Reponse'][($key-1)] = $tab_res[$key-1]->libelle_rep;
                      }
                      $i++;
                  }
                  $app->render('cours/cours.twig', array(
                      'cours' => $cours,
                      'titre' => Groupe::find($id)->titre_cou,
                      'groupe' => Cours::whereId_gro($id)->get(),
                      'qcm' => $qcm,
                      'fav' => Fav::whereId_com($_SESSION["id"])->whereId_gro($id)->count(),
                      'lvl' => Lvl::whereId_com($_SESSION["id"])->whereId_gro($id)->first()->nb_lvl,
                      'bsuiv' => $butt_suiv,
                      'bprec' => $butt_prec
                  ));
              }
              else $app->redirect('../');

          });

##############################################################################

/* 
    AFFICHAGE D'un CHAPITRE D'UN COURS 
*/
$app->get('/cours/:id/chapitre/:id_chap', function ($id, $id_chap) use ($app)
          {
              // variable permettant d'indiquer les erreurs occasionnées
              // FALSE si il n'y a pas d'erreur
              $erreur = NULL;

              // Permet de masquer le button en fonction du boolean
              $butt_suiv = false;
              $butt_prec = false;
              if(verif_Connect() == false)
                  $app->redirect('../login');

              // Si on cherche un chapitre inéxistant (inférieur) (ex : chapitre 0)
              // On redirige vers le chapitre 1
              // On masque le bouton "précedant"
              if(1 > $id_chap)
              {
                  $app->redirect("1");
              }
              // Si on cherche le 1er chapitre
              // On masque le bouton "suivant"
              else if(1 == $id_chap)
              {
                  $butt_prec = true;
              }

              // Si on cherche un chapitre non débloqué (supérieur)
              // On redirige vers le dernier chapitre débloqué
              if(Lvl::whereId_com($_SESSION["id"])->whereId_gro($id)->first()->nb_lvl < $id_chap)
              {
                  $id_chap --;
                  $app->redirect($id_chap."?e=fakelvl");
              }
              // Si le chapitre cherché est le dernier chapitre débloqué
              // On masque le bouton "suivant"
              if(Lvl::whereId_com($_SESSION["id"])->whereId_gro($id)->first()->nb_lvl == $id_chap)
              {
                  $butt_suiv = true;
              }

              // Récupération de l'erreur si elle éxiste
              if(isset($_GET["e"]) && $_GET["e"] == "fakelvl")
                  $erreur = "Il faut débloquer ce chapitre pour accèder au prochain !";

              $qcm = array();
              $cours = Cours::whereId_gro($id)->whereLvl($id_chap)->first();
              $i = 0;

              if($cours != NULL)
              {
                  foreach(QCM::whereId_cou($cours->id_cou)->get() as $key)
                  {
                      $qcm[$i]['question'] = $key->question_qcm;
                      $qcm[$i]['numrep'] = $key->id_qcm;
                      $qcm[$i]['numquestion'] = $key->num_qcm;
                      $ind = 0;
                      foreach($r = Rep::whereId_qcm($key->id_qcm)->get() as $key)
                      {
                          $qcm[$i]['Reponse'.$ind] = $key->libelle_rep;
                          $ind++;
                      }
                      $i++;
                  }
                  $app->render('cours/cours_chap.twig', array(
                      'cours' => $cours,
                      'titre' => Groupe::find($id)->titre_cou,
                      'groupe' => Cours::whereId_gro($id)->get(),
                      'qcm' => $qcm,
                      'fav' => Fav::whereId_com($_SESSION["id"])->whereId_gro($id)->count(),
                      'lvl' => Lvl::whereId_com($_SESSION["id"])->whereId_gro($id)->first()->nb_lvl,
                      'erreur' => $erreur,
                      'bsuiv' => $butt_suiv,
                      'bprec' => $butt_prec
                  ));
              }
              else $app->redirect('../');

          });

//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                QCM   //
//////////////////////////////////////////////////////////////////////////////////////////

/* 
    CREATION D'UN QCM - Methode GET
*/
$app->get('/qcm/create/:id', function ($id) use ($app)
          {
              if(verif_Connect() == false)
                  $app->redirect('../login');
              if(verif_EtatCompte('Professeur'))
                  $app->render('qcm/create.twig');
              else $app->redirect('../');
          });

/* 
    CREATION D'UN QCM - Methode POST
*/
$app->post('/qcm/create/:id', function ($id) use ($app)
           {
               require_once 'control/cours.php';
           });

##############################################################################

/*
    CREATION DU RESULTAT QCM
*/
$app->post('/cours/:id/resultat', function ($id_cou) use ($app)
           {
               require_once 'control/qcm.php';
           });


//////////////////////////////////////////////////////////////////////////////////////////
//                                                                              NOTES   //
//////////////////////////////////////////////////////////////////////////////////////////

$app->get('/user/:id/notes',function ($id) use ($app){

    $app->render('qcm/note.twig',array('note'=> Notes::whereid_com($id)->get(),'cours'=> Cours::get(),'groupe'=> Groupe::get()));

});


//////////////////////////////////////////////////////////////////////////////////////////
//                                                                            MATIERE   //
//////////////////////////////////////////////////////////////////////////////////////////

/* 
    CREATION D'UNE MATIERE - Méthode GET
*/
$app->get('/matiere/create', function () use ($app)
          {
              if(verif_Connect() == false)
                  $app->redirect('../login');
              if(verif_EtatCompte('Professeur'))
                  $app->render('matiere/create.twig');
              else $app->redirect('../');
          });

/* 
    CREATION D'UNE MATIERE - Méthode POST
*/
$app->post('/matiere/create', function () use ($app)
           {
               require_once 'control/matiere.php';
           });

##############################################################################

/* 
    AFFICHAGE D'UNE MATIERE 
*/
$app->get('/matiere/:nom', function ($nom) use ($app)
          {
              if(verif_Connect() == false)
                  $app->redirect('../login');
              $m = Matiere::whereLibelle_mat($nom)->first();
              $c = Cat::whereId_mat($m->id_mat)->get();
              $app->render('matiere/categorie.twig', array('mat' => $m, 'categorie' => $c, 'cours' => Groupe::get()));
          });


//////////////////////////////////////////////////////////////////////////////////////////
//                                                                          Categorie   //
//////////////////////////////////////////////////////////////////////////////////////////

/* 
    CREATION D'UNE CATEGORIE - Méthode GET 
*/
$app->get('/categorie/create', function () use ($app)
          {
              if(verif_Connect() == false)
                  $app->redirect('../login');
              if(verif_EtatCompte('Professeur'))
                  $app->render('categorie/create.twig', array('matieres' => Matiere::orderBy('libelle_mat')->get()));
              else $app->redirect('../');
          });

/* 
    CREATION D'UNE CATEGORIE - Méthode POST 
*/
$app->post('/categorie/create', function () use ($app)
           {
               require_once 'control/categorie.php';
           });


//////////////////////////////////////////////////////////////////////////////////////////
//                                                                            FAVORIS   //
//////////////////////////////////////////////////////////////////////////////////////////

/* 
    AJOUT/SUPPRESSION D'UN FAVORIS
*/
$app->get('/favoris/:action/:idu/:idc', function ($action, $idu, $idc) use ($app)
          {
              if(verif_Connect() == false)
                  $app->redirect('../login');

              function addFav($idu, $idc)
              {
                  $f = new Fav();
                  $f->id_com = $idu;
                  $f->id_gro = $idc;
                  $f->push();
              }

              function suppFav($idu, $idc)
              {
                  $f = Fav::whereId_com($idu)->whereId_gro($idc)->first();
                  $f->delete();
              }

              if($action == "delete")
              {
                  suppFav($idu, $idc);
                  $app->redirect("../../../");
              }
              if($action == "add")
                  addFav($idu, $idc);

              if($action == "del")
                  suppFav($idu, $idc);
              $app->redirect("../../../cours/$idc");
          });


//////////////////////////////////////////////////////////////////////////////////////////
//                                                                          Recherche   //
//////////////////////////////////////////////////////////////////////////////////////////

/*
    RECHERCHE D'UN COURS/CATEGORIE/MATIERE 
*/
$app->post('/cherche', function () use ($app)
           {
               $r=$_POST['cherche'];
               $m = Matiere::where('Libelle_mat','LIKE','%'.$r.'%')->get();
               $cat = Cat::where('libelle_cat','LIKE','%'.$r.'%')->get();
               $c = Groupe::where('titre_cou','LIKE','%'.$r.'%')->get();
               $app->render('recherche.twig',array('recherche'=>$r, 'mats' => $m, 'cats' => $cat, 'cours' => $c));
           });


//////////////////////////////////////////////////////////////////////////////////////////
//                                                                            DONNEES   //
//////////////////////////////////////////////////////////////////////////////////////////

/*
    JEUX DE DONNEES UTILE A L'AJAX POUR INSERER LES CATEGORIES EN FONCTION DE LA MATIERE
*/
$app->get('/donnees/cat/:nom', function ($nom) use ($app)
          {
              $m = Matiere::whereLibelle_mat($nom)->first();
              echo Cat::whereId_mat($m->id_mat)->get();
          });


//////////////////////////////////////////////////////////////////////////////////////////
//                                                                     ADMINISTRATION   //
//////////////////////////////////////////////////////////////////////////////////////////

/*
    PANEL D'ADMINISTRATION
*/
$app->get('/adm', function () use ($app)
          {
              if(!verif_Admin())
                  $app->redirect('./');
              $app->render('adm/index.twig', array(
                  'groupe' => Groupe::get(), 
                  'members' => Compte::get(), 
                  'etat' => ECompte::get(), 
                  'list_admin' => Admin::get(),
                  'cours' => Cours::get(),
                  'cat' => Cat::get(),
                  'mat' => Matiere::get()
              ));
          });

/*
    PERMET DE MODIFIER/SUPPRIMER UN COMPTE
*/
$app->get('/adm/compte/:id/:action', function ($id, $action) use ($app)
          {
              if(!verif_Admin())
                  $app->redirect('../../../../public');
              if($action == "del")
              {
                  $c = Compte::whereId_com($id)->first();
                  $etat = ECompte::whereId_etc($c->id_etc)->first();
                  $etat->delete();
                  $c->delete();
                  $app->redirect("../../../adm");
              }
              if($action == "edit")
              {
                  $app->render('adm/compte_edit.twig', array('compte' => Compte::whereId_com($id)->first()));
              }
          });

/*
    TRAITEMENT DE LA MODIFICATION D'UN COMPTE
*/
$app->post('/adm/compte/:id/edit', function ($id) use ($app)
           {
               require_once 'control/adm.php';
           });

/*
    AJOUT D'UN ADMIN
*/
$app->post('/adm/addAdm', function () use ($app)
           {
               require_once 'control/adm.php';
           });

/*
    SUPPRESSION D'UN ADMIN
*/
$app->post('/adm/suppAdm', function () use ($app)
           {
               require_once 'control/adm.php';
           });

/*
    PERMET DE SUPPRIMER UN GROUPE
*/
$app->get('/adm/group/:id/del', function ($id) use ($app){
    if(!verif_Admin())
        $app->redirect('../../../../public');
    Groupe::whereId_gro($id)->delete();
    Fav::whereId_gro($id)->delete();
    Cours::whereId_gro($id)->delete();
    Lvl::whereId_gro($id)->delete();

    $app->redirect("../../../adm");
});

?>
